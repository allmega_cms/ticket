<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\TicketBundle\Controller\{TicketController, TicketStateController};
use Allmega\TicketBundle\Entity\{Ticket, TicketState};
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\TicketBundle\Data;

class TicketStateControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $id = $this->createTicket()->getId();
        $this->runTests(
            routeName: $this->add,
            role: Data::TICKET_AUTHOR_ROLE,
            routeParams: ['id' => $id]);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::TICKET_AUTHOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testState(): void
    {
        $redirectRoute = TicketController::ROUTE_NAME . $this->show;
        $this->runModifyTests(
            role: Data::TICKET_AUTHOR_ROLE,
            redirectRoute: $redirectRoute,
            fnRedirectParams: 'getRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $redirectRoute = TicketController::ROUTE_NAME . $this->show;
        $this->runModifyTests(
            role: Data::TICKET_AUTHOR_ROLE,
            delete: true,
            redirectRoute: $redirectRoute,
            fnRedirectParams: 'getRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $ticket = $this->createTicket();
        $entity = TicketState::build(ticket: $ticket, author: $ticket->getAuthor());
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return TicketStateController::ROUTE_NAME . $name;
    }

    /**
     * @throws ClassNotFoundException
     */
    private function createTicket(): Ticket
    {
        $user = $this->findUserByRole(Data::TICKET_AUTHOR_ROLE);
        $ticket = Ticket::build(author: $user, editors: [$user]);
        $this->em->persist($ticket);
        $this->em->flush();

        return $ticket;
    }

    protected function getRouteParams(): array
    {
        return ['id' => $this->params->getEntity()->getTicket()->getId()];
    }
}