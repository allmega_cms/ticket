<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\TicketBundle\Controller\TicketController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Utils\Params\UTypes;
use Allmega\TicketBundle\Entity\Ticket;
use Allmega\TicketBundle\Data;

class TicketControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, Data::TICKET_USER_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testSearch(): void
    {
        $this->runTests($this->search, Data::TICKET_USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::TICKET_USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests($this->show, Data::TICKET_AUTHOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::TICKET_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::TICKET_AUTHOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(
            role: Data::TICKET_AUTHOR_ROLE,
            delete: true,
            redirectParams: ['id' => UTypes::AUTHOR]);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testArchive(): void
    {
        $this->runModifyTests(
            routeName: 'archive',
            role: Data::TICKET_AUTHOR_ROLE,
            redirectParams: ['id' => UTypes::AUTHOR],
            redirectName: $this->index);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $user = $this->findUserByRole(Data::TICKET_AUTHOR_ROLE);
        $entity = Ticket::build(author: $user);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return TicketController::ROUTE_NAME . $name;
    }
}