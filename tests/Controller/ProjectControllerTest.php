<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\TicketBundle\Controller\ProjectController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Utils\Params\UTypes;
use Allmega\TicketBundle\Entity\Project;
use Allmega\TicketBundle\Data;

class ProjectControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, Data::PROJECT_USER_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testSearch(): void
    {
        $this->runTests($this->search, Data::PROJECT_USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::PROJECT_USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests($this->show, Data::PROJECT_USER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, role: Data::PROJECT_USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::PROJECT_USER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testState(): void
    {
        $this->runModifyTests(
            role: Data::PROJECT_USER_ROLE,
            fnRedirectParams: 'getRedirectParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(
            role: Data::PROJECT_USER_ROLE,
            delete: true,
            redirectParams: ['id' => UTypes::AUTHOR]);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $user = $this->findUserByRole(Data::PROJECT_USER_ROLE);
        $entity = Project::build(user: $user, employees: [$user]);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return ProjectController::ROUTE_NAME . $name;
    }

    protected function getRedirectParams(): array
    {
        return ['id' => $this->params->getEntity()->getId(), 'type' => UTypes::AUTHOR];
    }
}