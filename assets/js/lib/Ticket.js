export default class Ticket
{
    #reportId = '#ticket_report'
    #doneId = '#ticket_done'
    #report
    #done

    constructor()
    {
        this.#init().#registerEvents()
    }

    #registerEvents()
    {
        /** See the Events.js in blog package */
        $(document).on('app.content.loaded', () => this.#init())
        $(document).on('change', this.#doneId, () => this.#doChange())
    }

    #init()
    {
        this.#done = $(this.#doneId)
	    if(this.#done.length) {
            this.#report = $(this.#reportId).parent()
            this.#doChange()
        }
        return this
    }

	#doChange()
    {
        this.#done.prop('checked') ? this.#report.show() : this.#report.hide()
    }
}