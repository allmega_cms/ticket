<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle;

use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntriesMap, ControllerEntry};
use Allmega\TicketBundle\Controller\{ProjectController, TicketController};
use Allmega\BlogBundle\Utils\Loader\Entries\MenupointEntry;
use Allmega\TicketBundle\Entity\Project;
use Allmega\BlogBundle\Model\PackageData;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\BlogBundle\Entity\Item;

class Data extends PackageData
{
	public const DOMAIN = 'AllmegaTicketBundle';
	public const PACKAGE = 'ticket';

    public const NOTIFICATION_TICKET_CREATED = 'ticket_created';
    public const NOTIFICATION_TICKET_ASSIGNED = 'ticket_assigned';
    public const NOTIFICATION_TICKET_COMPLETED = 'ticket_completed';
    public const NOTIFICATION_TICKET_STATE_CHANGED = 'ticket_state_changed';
    public const NOTIFICATION_PROJECT_ENABLED = 'ticket_project_enabled';
    public const NOTIFICATION_ABSENT_EMPLOYEES = 'ticket_absent_employees';

	public const GROUP_TYPE_PROJECT = 'ticket.project';
    public const GROUP_TYPE_TICKET = 'ticket.main';

    public const PROJECT_MANAGER_GROUP = 'project.manager';
    public const PROJECT_AUTHOR_GROUP = 'project.author';
    public const PROJECT_USER_GROUP = 'project.user';

    public const PROJECT_MANAGER_ROLE = 'ROLE_PROJECT_MANAGER';
    public const PROJECT_AUTHOR_ROLE = 'ROLE_PROJECT_AUTHOR';
    public const PROJECT_USER_ROLE = 'ROLE_PROJECT_USER';

	public const TICKET_MANAGER_GROUP = 'ticket.manager';
	public const TICKET_AUTHOR_GROUP = 'ticket.author';
    public const TICKET_USER_GROUP = 'ticket.user';

	public const TICKET_MANAGER_ROLE = 'ROLE_TICKET_MANAGER';
	public const TICKET_AUTHOR_ROLE = 'ROLE_TICKET_AUTHOR';
	public const TICKET_USER_ROLE = 'ROLE_TICKET_USER';

	protected function setRegisterData(): void
	{
        $this->package = self::PACKAGE;
        $this->data = [
            'controllerEntries' => $this->getControllerEntries(),
            'webpackEntries' => $this->getWebpackEntries(),
        ];
	}

    protected function setLoadData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'notificationtypes' => $this->getNotificationTypes(),
            'menupoints' => $this->getMenuPoints(),
            'grouptypes' => $this->getGroupTypes(),
            'groups' => $this->getGroups(),
            'roles' => $this->getRoles(),
            'users' => $this->getUsers(),
            'items' => $this->getItems(),
        ];
    }

	/**
	 * @return array<int,ControllerEntry>
	 */
	protected function getControllerEntries(): array
	{
		return [
            new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'project',
                ProjectController::class,
                'getDashboardWidget'),
			new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'ticket',
                TicketController::class,
                'getDashboardWidget'),
		];
	}

	/**
	 * @return array<string,MenupointEntry>
	 */
	protected function getMenuPoints(): array
	{
        $projectRoute = ProjectController::ROUTE_NAME . 'index';
        $ticketRoute = TicketController::ROUTE_NAME . 'index';

        $project = 'project.main';
		$ticket = 'ticket.main';

		$menuPoints = [
            new MenupointEntry('project.manager', $projectRoute, ['id' => 'manager'], $this->routeType, 3, [self::PROJECT_MANAGER_GROUP], $project),
            new MenupointEntry('project.author', $projectRoute, ['id' => 'author'], $this->routeType, 2, [self::PROJECT_AUTHOR_GROUP], $project),
            new MenupointEntry('project.member', $projectRoute, [], $this->routeType, 1, [self::PROJECT_USER_GROUP], $project),
            new MenupointEntry($project, '', [], $this->menuType, 8, [], BlogData::MENUPOINT_BLOG_MANAGE),
            new MenupointEntry('ticket.manager', $ticketRoute, ['id' => 'manager'], $this->routeType, 3, [self::TICKET_MANAGER_GROUP], $ticket),
            new MenupointEntry('ticket.author', $ticketRoute, ['id' => 'author'], $this->routeType, 2, [self::TICKET_AUTHOR_GROUP], $ticket),
			new MenupointEntry('ticket.member', $ticketRoute, [], $this->routeType, 1, [self::TICKET_USER_GROUP], $ticket),
			new MenupointEntry($ticket, '', [], $this->menuType, 9, [], BlogData::MENUPOINT_BLOG_MANAGE),
		];
		return [self::PACKAGE => $this->setSysAndActive($menuPoints)];
	}

    /**
     * @return array<int,Item>
     */
    protected function getItems(): array
    {
        $items = [
            Item::build(
                Project::class,
                ProjectController::ROUTE_NAME . 'show',
                'project.item.description',
                'project.item.label'),
        ];
        return [self::PACKAGE => $items];
    }

    protected function getAuthData(): array
    {
        return [
            self::GROUP_TYPE_PROJECT => [
                self::PROJECT_MANAGER_GROUP => [
                    self::PROJECT_MANAGER_ROLE => 'project.manager',
                    self::PROJECT_AUTHOR_ROLE => 'project.author',
                    self::PROJECT_USER_ROLE => 'project.user',
                ],
                self::PROJECT_AUTHOR_GROUP => [
                    self::PROJECT_AUTHOR_ROLE => 'project.author',
                    self::PROJECT_USER_ROLE => 'project.user',
                ],
                self::PROJECT_USER_GROUP => [
                    self::PROJECT_USER_ROLE => 'project.user',
                ],
            ],
            self::GROUP_TYPE_TICKET => [
                self::TICKET_MANAGER_GROUP => [
                    self::TICKET_MANAGER_ROLE => 'ticket.manager',
                    self::TICKET_AUTHOR_ROLE => 'ticket.author',
                    self::TICKET_USER_ROLE => 'ticket.user',
                ],
                self::TICKET_AUTHOR_GROUP => [
                    self::TICKET_AUTHOR_ROLE => 'ticket.author',
                    self::TICKET_USER_ROLE => 'ticket.user',
                ],
                self::TICKET_USER_GROUP => [
                    self::TICKET_USER_ROLE => 'ticket.user',
                ],
            ],
        ];
    }

    protected function getNotificationTypesData(): array
    {
        return [
            self::NOTIFICATION_TICKET_CREATED,
            self::NOTIFICATION_TICKET_ASSIGNED,
            self::NOTIFICATION_TICKET_COMPLETED,
            self::NOTIFICATION_TICKET_STATE_CHANGED,
            self::NOTIFICATION_PROJECT_ENABLED,
            self::NOTIFICATION_ABSENT_EMPLOYEES,
        ];
    }
}