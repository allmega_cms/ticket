<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Manager;

use Allmega\TicketBundle\Entity\Ticket;
use Allmega\TicketBundle\{Data, Events};
use Allmega\MediaBundle\Events as MediaEvents;
use Allmega\TicketBundle\Controller\TicketController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\TicketBundle\Form\{TicketType, TicketBaseType};
use Symfony\Component\HttpFoundation\Response;

trait TicketControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $this->handleTicket();
                break;
            case $this->delete:
                $this->params->getEntity()->getTags()->clear();
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        $mediaDir = $this->params->getEntity()->getMediaDir();
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $this->saveFiles($mediaDir, $this->getFormFiles());
                break;
            case $this->delete:
                $this->params
                    ->addArguments(['folder' => $mediaDir])
                    ->setEventName(MediaEvents::DELETE_FILES);
                $this->dispatchEvent();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        $this->params->addRouteParams(['id' => 'author']);
        return $this;
    }

    private function handleTicket(): void
    {
        if ($this->params->getAction() === $this->add) {
            $this->params
                ->getEntity()
                ->setNum('#' . uniqid())
                ->setAuthor($this->getUser());
        }

        $this->params->getEventName() === Events::TICKET_MANAGE ? 
            $this->setTicketAssigned()->setTicketDone() : 
            $this->addTicketEditors();
    }

    private function addTicketEditors(): void
    {
        $departments = $this->form['departments']->getData();
        foreach ($departments as $department) {
            foreach ($department->getUsers() as $editor) {
                if ($editor->isEnabled()) {
                    $this->params->getEntity()->addEditor($editor);
                }
            }
        }
    }

    private function setTicketAssigned(): static
    {
        $ticket = $this->params->getEntity();
        if (!$ticket->getEditors()->count()) $ticket->setAssigned(false);
        elseif (!$ticket->getAssigned() && !$ticket->getDone()) {
            $ticket->setAssigned(true);
            $this->params->setEventName(Events::TICKET_ASSIGNED);
            $this->dispatchEvent();
        }
        return $this;
    }

    private function setTicketDone(): void
    {
        $ticket = $this->params->getEntity();
        if (!$ticket->getDone()) $ticket->setReporter(null);
        else {
            $ticket->setReporter($this->getUser())->setReported();
            $this->params->setEventName(Events::TICKET_COMPLETED);
            $this->dispatchEvent();
        }
    }

    private function save(Ticket $ticket = null, array $arguments = []): Response
    {
        $eventName = $ticket ? Events::TICKET_UPDATED : Events::TICKET_CREATED;
        $formType = TicketBaseType::class;
        $ticket = $ticket ?? new Ticket();

        $formParams = ['item' => $ticket];
        $author = $ticket->getAuthor();

        $user = $this->getUser();
        if ($author && $author->getId() !== $user->getId()) {
            $eventName = Events::TICKET_MANAGE;
            $formParams['showDelete'] = false;
            $formType = TicketType::class;
        }
        
        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            entity: $ticket,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: $formType,
            routeName: TicketController::ROUTE_NAME,
            templatesPath: TicketController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);      
    }
}