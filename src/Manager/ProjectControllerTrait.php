<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Manager;

use Allmega\TicketBundle\{Data, Events};
use Allmega\TicketBundle\Entity\Project;
use Allmega\TicketBundle\Form\ProjectType;
use Allmega\BlogBundle\Utils\Params\UTypes;
use Allmega\MediaBundle\Events as MediaEvents;
use Allmega\TicketBundle\Controller\ProjectController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait ProjectControllerTrait
{
    protected function preExecution(): static
    {
        $project = $this->params->getEntity();
        $user = $this->getUser();

        switch ($this->params->getAction()) {
            case $this->add:
                $project->setCreator($user)->setEditor($user);
                break;
            case $this->state:
            case $this->edit:
                $project->setEditor($user)->setUpdated();
                break;
            case $this->delete:
                $this->clearProject();
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $this->handleProjectFilesAndState();
                break;
            case $this->delete:
                $this->deleteProjectFiles();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $id = $this->params->getEntity()->getId();
                $this->params
                    ->addRouteParams(['id' => $id, 'type' => UTypes::AUTHOR])
                    ->setRouteName($this->params->getRouteShort(), $this->show);
                break;
            case $this->delete:
                $this->params->addRouteParams(['id' => UTypes::AUTHOR]);
                break;
            default:
        }
        return $this;
    }

    private function clearProject(): void
    {
        $project = $this->params->getEntity();
        $em = $this->getEntityManager();

        foreach ($project->getTicketunits() as $unit) {
            $unit->setProject(null);
            $em->persist($unit);
        }
        $em->flush();
        
        $project->getEmployees()->clear();
        $project->getTags()->clear();
    }

    private function handleProjectFilesAndState(): static
    {
        $project = $this->params->getEntity();
        if ($this->params->getAction() != $this->state) {
            $this->saveFiles($project->getMediaDir(), $this->getFormFiles());
        }

        if ($project->isActive()) {
            $this->params->setEventName(Events::PROJECT_ENABLED);
            $this->dispatchEvent();
        }
        return $this;
    }

    private function deleteProjectFiles(): void
    {
        $this->params
            ->addArguments(['folder' => $this->params->getEntity()->getMediaDir()])
            ->setEventName(MediaEvents::DELETE_FILES);
        $this->dispatchEvent();
    }

    private function save(Project $project = null, array $arguments = []): Response
    {
        $eventName = $project ? Events::PROJECT_UPDATED : Events::PROJECT_CREATED;
        $project = $project ?? new Project();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $project],
            entity: $project,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: ProjectType::class,
            routeName: ProjectController::ROUTE_NAME,
            templatesPath: ProjectController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);      
    }
}