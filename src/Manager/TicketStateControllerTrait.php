<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Manager;

use Allmega\TicketBundle\{Data, Events};
use Allmega\TicketBundle\Form\TicketStateType;
use Allmega\TicketBundle\Controller\TicketController;
use Allmega\TicketBundle\Entity\{Ticket, TicketState};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\TicketBundle\Controller\TicketStateController;
use Symfony\Component\HttpFoundation\Response;

trait TicketStateControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $this->handleTicketState();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        $id = $this->params->getEntity()->getTicket()->getId();
        $this->params
            ->setRouteName(TicketController::ROUTE_NAME, $this->show)
            ->addRouteParams(['id' => $id]);
            return $this;
    }

    private function handleTicketState(): void
    {
        $ticketState = $this->params->getEntity()->setUpdated();
        $this->params->getAction() == $this->add ? 
            $ticketState->setAuthor($this->getUser()) :
            $ticketState->setReadedAt(true);
    }

    private function save(TicketState $state = null, Ticket $ticket = null, array $arguments = []): Response
    {
        $item = $ticket ?? $state;
        $ticket = $ticket ?? $state->getTicket();
        $formParams = $this->buildFormParams($ticket, $item);

        $eventName = $state ? Events::TICKETSTATE_UPDATED : Events::TICKETSTATE_CREATED;
        $state = $state ?? (new TicketState())->setTicket($ticket);
        
        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            entity: $state,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: TicketStateType::class,
            routeName: TicketStateController::ROUTE_NAME,
            templatesPath: TicketStateController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);      
    }

    private function buildFormParams(Ticket $ticket, Ticket|TicketState $item): array
    {
        return [
            'link' => [
                'route' => TicketController::ROUTE_NAME . 'show',
                'params' => ['id' => $ticket->getId()],
                'title' => 'ticket.show'
            ],
            'item' => $item
        ];
    }
}