<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle;

final class Events
{
    public const PROJECT_ENABLED = 'project.enabled';
    public const PROJECT_CREATED = 'project.created';
    public const PROJECT_UPDATED = 'project.updated';
    public const PROJECT_DELETED = 'project.deleted';
    public const PROJECT_STATE_CHANGED = 'project.state_changed';

    public const TICKET_CREATED = 'ticket.created';
    public const TICKET_UPDATED = 'ticket.updated';
    public const TICKET_DELETED = 'ticket.deleted';
    public const TICKET_MANAGE = 'ticket.manage';
    public const TICKET_ARCHIVED = 'ticket.archived';
    public const TICKET_ASSIGNED = 'ticket.assigned';
    public const TICKET_COMPLETED = 'ticket.completed';

    public const TICKETSTATE_CREATED = 'ticketstate.created';
    public const TICKETSTATE_UPDATED = 'ticketstate.updated';
    public const TICKETSTATE_DELETED = 'ticketstate.deleted';
}