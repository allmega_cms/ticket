<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Repository;

use Allmega\TicketBundle\Entity\TicketState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TicketStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicketState::class);
    }
}