<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Repository;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\Params\UTypes;
use Allmega\TicketBundle\Entity\Ticket;
use Allmega\BlogBundle\Utils\Params\RepoParams;
use Allmega\BlogBundle\Model\SearchableInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class TicketRepository extends ServiceEntityRepository implements SearchableInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    public function findAllQuery(RepoParams $params): Query
    {
        $query = $this->createQueryBuilder('a');

        if ($params->getCount()) $query->select('COUNT(a.id)');
        elseif ($params->getLimit() > 0) $query->setMaxResults($params->getLimit());

        $this->modifyQuery($query, $params->getType(), $params->getUser()->getId());
        return $query->orderBy('a.created', 'DESC')->getQuery();
    }

    public function findByRepoParams(RepoParams $params): int|array
    {
        $query = $this->findAllQuery($params);
        return $params->getCount() ? $query->getSingleScalarResult() : $query->getResult();
    }

    public function countLatest(User $user): int
    {
        return $this->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->join('t.editors', 'u')
            ->where('u.id = :id')
            ->andWhere('t.created BETWEEN :last AND :now')
            ->setParameter('last', $user->getLastSeen()->format('Y-m-d H:i:S'))
            ->setParameter('now', (new \DateTime())->format('Y-m-d H:i:s'))
            ->setParameter('id', $user->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (count($terms) < 1) return [];
        
        extract($options);
        //[$user, $type]

        $query = $this->createQueryBuilder('a');

        foreach ($terms as $key => $term) {
            $query
                ->orWhere('a.num LIKE :n_'.$key)
                ->setParameter('n_'.$key, '%'.$term.'%')
                ->orWhere('a.title LIKE :f_'.$key)
                ->setParameter('f_'.$key, '%'.$term.'%')
                ->orWhere('a.description LIKE :l_'.$key)
                ->setParameter('l_'.$key, '%'.$term.'%')
                ->orWhere('a.report LIKE :r_'.$key)
                ->setParameter('r_'.$key, '%'.$term.'%');
        }

        $this->modifyQuery($query, $type, $user->getId());
        return $query
            ->orderBy('a.title', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    private function modifyQuery(QueryBuilder &$query, string $type, string $uid): void
    {
        switch ($type) {
            case UTypes::AUTHOR:
                $query
                    ->andWhere('a.author = :id')
                    ->setParameter('id', $uid);
                break;
            case UTypes::ARCHIVE:
                $query
                    ->andWhere('a.author = :id')
                    ->setParameter('id', $uid)
                    ->andWhere('a.archived = 1');
                break;
            case UTypes::MEMBER:
                $query
                    ->join('a.editors', 'u')
                    ->andWhere('u.id = :id')
                    ->setParameter('id', $uid)
                    ->andWhere('a.done = 0');
                break;
            default:
        }
    }
}