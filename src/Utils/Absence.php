<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Utils;

use Allmega\TicketBundle\Data;
use Allmega\TicketBundle\Entity\Ticket;
use Allmega\CalendarBundle\Entity\{CalendarUnit, CalendarUnitType};
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

class Absence
{
    private array $units = [];
    public function __construct(private TranslatorInterface $translator, private EntityManagerInterface $em) {}
    
    public function getPhrase(Ticket $ticket): string
    {
        $phrase = '';
        $employees = $this->getAbsentEmployees($ticket);
        
        if ($employees) {
            $params = ['%employees%' => "\n - " . implode("\n - ", $employees)];
            $phrase = $this->translator->trans('label.absent_employees', $params, Data::DOMAIN);
        }

        return $phrase;
    }

    public function getAbsentEmployees(Ticket $ticket): array
    {
        $employees = [];

        if (!$this->units) $this->setUnits();

        foreach ($this->units as $unit) {
            $uid = $unit->getEmployment()->getUser()->getId();
            foreach ($ticket->getEditors() as $user) {
                if ($uid == $user->getId()) $employees[] = $user->getFullname();
            }
        }

        return $employees;
    }

    private function setUnits(): void
    {
        if (class_exists(CalendarUnit::class)) {
            $this->units = $this->em
                ->getRepository(CalendarUnit::class)
                ->findForNowByUnitTypes(CalendarUnitType::$absenceUnits);
        } 
    }
}