<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Utils;

use Allmega\BlogBundle\Utils\Params\TemplateParams as BlogTemplateParams;
use Allmega\BlogBundle\Utils\Params\UTypes;

class TemplateParams extends BlogTemplateParams
{
    private string $archiveLabel;

    public function __construct(string $type, bool $show = false)
    {
        parent::__construct('ticket', $type, $show);
        switch ($type) {
            case UTypes::ARCHIVE:
                $this->archiveLabel = 'ticket.unarchive';
                break;
            case UTypes::AUTHOR:
                $this->archiveLabel = 'ticket.archive';
                break;
            default:
                $this->archiveLabel = '';
        }
    }

    public function getArchiveLabel(): string
    {
        return $this->archiveLabel;
    }
}