<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @package   Allmega
 * @copyright Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Utils\Twig;

use Allmega\TicketBundle\Entity\Ticket;
use Twig\Environment;

class CreatorInfo
{
    private string $datetype;
    private string $timetype;
    private ?string $border;
    private Ticket $ticket;
    private bool $state;
    private string $bg;

    public function __construct(private readonly Environment $env, array $params)
    {
        $datetype = 'medium';
        $timetype = 'short';
        $state = false;
        $border = null;
        $bg = 'light';

        extract($params);

        $this->datetype = $datetype;
        $this->timetype = $timetype;
        $this->ticket = $ticket;
        $this->border = $border;
        $this->state = $state;
        $this->bg = $bg;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaTicket/twig/_creator_info.html.twig', ['info' => $this]);
    }

	public function getState(): bool
    {
		return $this->state;
	}

	public function setState(bool $state): static
    {
		$this->state = $state;
		return $this;
	}

    public function getBorder(): ?string
    {
        return $this->border;
    }

    public function setBorder(string $border): static
    {
        $this->border = $border;
        return $this;
    }

    public function getDatetype(): string
    {
        return $this->datetype;
    }

    public function setDatetype(string $datetype): static
    {
        $this->datetype = $datetype;
        return $this;
    }

    public function getTimetype(): string
    {
        return $this->timetype;
    }

    public function setTimetype(string $timetype): static
    {
        $this->timetype = $timetype;
        return $this;
    }

    public function getTicket(): Ticket
    {
        return $this->ticket;
    }

    public function setTicket(Ticket $ticket): static
    {
        $this->ticket = $ticket;
        return $this;
    }

    public function getBg(): string
    {
        return $this->bg;
    }
 
    public function setBg(string $bg): static
    {
        $this->bg = $bg;
        return $this;
    }
}