<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @package   Allmega
 * @copyright Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Utils\Twig;

use Allmega\TicketBundle\Entity\Ticket;
use Twig\Error\{LoaderError, RuntimeError, SyntaxError};
use Twig\Environment;

class TicketState
{
    private string $color;
    private Ticket $ticket;

    public function __construct(private readonly Environment $env, array $params)
    {
        extract($params);
        $this->color = $ticket->getDone() ? 'green' : 'red';
        $this->color = $ticket->getStates()->count() && !$ticket->getDone() ? 'yellow' : $this->color;
        $this->ticket = $ticket;
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function getView(): string
    {
        return $this->env->render('@AllmegaTicket/twig/_ticket_state.html.twig', ['state' => $this]);
    }

	public function getColor(): string
    {
		return $this->color;
	}

	public function setColor(string $color): static
    {
		$this->color = $color;
		return $this;
	}

	public function getTicket(): Ticket
    {
		return $this->ticket;
	}

	public function setTicket(Ticket $ticket): static
    {
		$this->ticket = $ticket;
		return $this;
	}
}