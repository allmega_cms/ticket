<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Form;

use Allmega\TicketBundle\Data;
use Allmega\BlogBundle\Utils\Priority;
use Allmega\AuthBundle\Model\UsersTrait;
use Allmega\TicketBundle\Entity\Project;
use Allmega\MediaBundle\Form\FileLoadType;
use Allmega\BlogBundle\Form\Type\TagsInputType;
use Allmega\AuthBundle\Repository\UserRepository;
use Allmega\AuthBundle\Utils\Params\UsersTraitParams;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ProjectType extends AbstractType
{
    use UsersTrait;

    public function __construct(
        private readonly UsersTraitParams $usersParams,
        private readonly UserRepository $userRepo) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        extract($this->createOptions());
        // [$usersOptions]
        $project = $builder->getData();

        $builder
            ->add('parent', EntityType::class, [
                'label' => 'project.label.parent',
                'help' => 'project.help.parent',
                'required' => false,
                'class' => Project::class,
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $er) use ($project) {
                    $query = $er->createQueryBuilder('p')
                        ->where('p.active = 1')
                        ->orderBy('p.title', 'ASC');

                    $pid = $project->getId();
                    if ($pid) {
                        $query
                            ->andWhere('p.id != :id')
                            ->setParameter('id', $pid);
                    }
                    return $query;
                },
            ])
            ->add('title', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'project.label.title'],
                'label' => 'project.label.title'
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['rows' => 5, 'placeholder' => 'project.label.description'],
                'label' => 'project.label.description'
            ])
            ->add('employees', EntityType::class, $usersOptions)
            ->add('active', null, [
                'label' => 'project.label.active',
                'help' => 'project.help.active'
            ])
            ->add('prio', ChoiceType::class, [
                'help' => 'label.priority.help',
                'label' => 'label.priority.name',
                'translation_domain' => 'AllmegaBlogBundle',
                'choices' => Priority::getPriorityChoices()
            ])
            ->add('tags', TagsInputType::class, [
                'attr' => ['placeholder' => 'project.label.tags'],
                'label' => 'project.label.tags',
                'help'  => 'project.help.tags',
                'required' => false
            ])
            ->add('files', FileLoadType::class, [
                'attr' => ['multiple' => true],
                'required' => false,
                'mapped' => false,
                'label' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }

    private function createOptions(): array
    {
        $this->usersParams
            ->setLabel('project.label.employees')
            ->setHelp('project.help.employees')
            ->setRoles([Data::PROJECT_USER_ROLE])
            ->setMultiple(true);

        $usersOptions = $this->getUsersOptions();
        return ['usersOptions' => $usersOptions];
    }
}