<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Form;

use Allmega\TicketBundle\Data;
use Allmega\TicketBundle\Entity\TicketState;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketStateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
	{
		$builder
			->add('message', TextareaType::class, [
				'attr'  => ['rows' => 5, 'placeholder' => 'ticketstate.label.message'],
				'label' => 'ticketstate.label.message',
				'help'  => 'ticketstate.help.message',
				'required' => false
			]);
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => TicketState::class,
            'translation_domain' => Data::DOMAIN
		]);
	}
}