<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Form;

use Allmega\TicketBundle\Data;
use Allmega\BlogBundle\Utils\Priority;
use Allmega\AuthBundle\Data as AuthData;
use Allmega\MediaBundle\Form\FileLoadType;
use Allmega\BlogBundle\Form\Type\TagsInputType;
use Allmega\TicketBundle\Entity\{Project, Ticket};
use Allmega\AuthBundle\Model\{GroupsTrait, UsersTrait};
use Allmega\AuthBundle\Repository\{GroupRepository, UserRepository};
use Allmega\AuthBundle\Utils\Params\{GroupsTraitParams, UsersTraitParams};
use Symfony\Component\Form\Extension\Core\Type\{TextareaType, ChoiceType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\ORM\EntityRepository;

class TicketBaseType extends AbstractType
{
    use GroupsTrait;
    use UsersTrait;

    public function __construct(
        private readonly GroupsTraitParams $groupsParams,
        private readonly UsersTraitParams $usersParams,
        private readonly GroupRepository $groupRepo,
        private readonly UserRepository $userRepo,
        private readonly Security $security) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        extract($this->createOptions());
        // [$groupsOptions, $usersOptions]
        $security = $this->security;

        if ($this->security->isGranted(Data::PROJECT_USER_GROUP)) {
            $builder
                ->add('project', EntityType::class, [
                    'label' => 'ticket.label.project',
                    'help' => 'ticket.help.project',
                    'required' => false,
                    'class' => Project::class,
                    'choice_label' => 'title',
                    'query_builder' => function (EntityRepository $er) use ($security) {
                        $uid = $security->getUser()->getId();
                        return $er->createQueryBuilder('p')
                            ->join('p.employees', 'u')
                            ->where('u.id = :uid')
                            ->setParameter('uid', $uid)
                            ->andWhere('p.active = 1')
                            ->orderBy('p.title', 'ASC');
                    }
                ]);
        }

        $builder
            ->add('title', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'ticket.label.title'],
                'label' => 'ticket.label.title'
            ])
            ->add('description', TextareaType::class, [
                'attr'  => ['rows' => 5, 'placeholder' => 'ticket.label.description'],
                'label' => 'ticket.label.description',
                'help'  => 'ticket.help.description',
                'help_html' => true,
                'required' => false
            ])
            ->add('departments', EntityType::class, $groupsOptions)
            ->add('editors', EntityType::class, $usersOptions)
            ->add('prio', ChoiceType::class, [
                'help' => 'label.priority.help',
                'label' => 'label.priority.name',
                'translation_domain' => 'AllmegaBlogBundle',
                'choices' => Priority::getPriorityChoices(),
            ])
            ->add('tags', TagsInputType::class, [
                'attr' => ['placeholder' => 'ticket.label.tags'],
                'label' => 'ticket.label.tags',
                'help'  => 'ticket.help.tags',
                'required' => false
            ])
            ->add('files', FileLoadType::class, [
                'attr' => ['multiple' => true],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
			->add('archived', null, [
				'label' => 'ticket.label.archived',
				'help'  => 'ticket.help.archived'
			]);

            $ticket = $builder->getData();
            if ($ticket->getDone()) {
                $builder->add('done', null, [
                    'label' => 'ticket.label.done',
                    'help'  => 'ticket.help.adone'
                ]);
            }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }

    private function createOptions(): array
    {
        $this->groupsParams
            ->setShortnames([AuthData::GROUP_TYPE_DEPARTMENT])
            ->setLabel('ticket.label.departments')
            ->setHelp('ticket.help.departments')
            ->setMultiple(true)
            ->setMapped(false);

        $groupsOptions = $this->getGroupsOptions();

        $this->usersParams
            ->setLabel('ticket.label.editors')
            ->setHelp('ticket.help.editors')
            ->setRoles([Data::TICKET_USER_ROLE])
            ->setMultiple(true);

        $usersOptions = $this->getUsersOptions();

        return [
            'groupsOptions' => $groupsOptions,
            'usersOptions' => $usersOptions,
        ];
    }
}