<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Form;

use Allmega\TicketBundle\Data;
use Allmega\TicketBundle\Entity\Ticket;
use Allmega\AuthBundle\Model\UsersTrait;
use Allmega\AuthBundle\Repository\UserRepository;
use Allmega\AuthBundle\Utils\Params\UsersTraitParams;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TicketType extends AbstractType
{
    use UsersTrait;

    public function __construct(
        private readonly UsersTraitParams $usersParams,
        private readonly UserRepository $userRepo) {}

	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
        extract($this->createOptions());
		$builder
			->add('editors', EntityType::class, $usersOptions)
			->add('done', null, [
				'label' => 'ticket.label.done',
				'help'  => 'ticket.help.done'
			])
			->add('report', TextareaType::class, [
				'attr'  => ['rows' => 5, 'placeholder' => 'ticket.label.report'],
				'label' => 'ticket.label.report',
				'help'  => 'ticket.help.report',
				'required' => false
			]);
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => Ticket::class,
            'translation_domain' => Data::DOMAIN
		]);
	}

    private function createOptions(): array
    {
        $this->usersParams
            ->setLabel('ticket.label.editors')
            ->setHelp('ticket.help.editors')
            ->setRoles([Data::TICKET_USER_ROLE])
            ->setMultiple(true);

        $usersOptions = $this->getUsersOptions();
        return ['usersOptions' => $usersOptions];
    }
}