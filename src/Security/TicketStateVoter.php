<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Security;

use Allmega\TicketBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\TicketBundle\Entity\{Ticket, TicketState};
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TicketStateVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'ticket-state');
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        $isUser = $this->hasRole($user, Data::TICKET_USER_ROLE);
        if (!$isUser || !$this->isSettedAndSupports($attribute, $subject)) return false;
        
        switch($attribute) {
            case $this->add:
                // Add states for ticket can only editors of ticket
                $result = $subject->getEditors()->contains($user);
                break;
            case $this->state:
                $result = $this->isSameUser($user, $subject->getTicket()->getAuthor());
                break;
            case $this->delete:
            case $this->edit:
                $result = $this->isSameUser($user, $subject->getAuthor());
                break;
            default:
                $result = false;
        }
        
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof TicketState || $subject instanceof Ticket;
    }
}