<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Security;

use Allmega\TicketBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\TicketBundle\Entity\Ticket;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TicketVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;
    
    protected string $archive = 'archive';

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'ticket', [$this->archive]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        $isUser = $this->hasRole($user, Data::TICKET_USER_ROLE);
        if (!$isUser || !$this->isSettedAndSupports($attribute, $subject)) return false;

        $isSameUser = $subject && $this->isSameUser($user, $subject->getAuthor());
        $isEditor = $subject && $subject->getEditors()->contains($user);

        $isManager = $this->hasRole($user, Data::TICKET_MANAGER_ROLE);
        $isAuthor = $this->hasRole($user, Data::TICKET_AUTHOR_ROLE);

        switch ($attribute) {
            case $this->dashboard:
            case $this->search:
            case $this->list:
                $result = true;
                break;
            case $this->add:
                $result = $isAuthor;
                break;
            case $this->show:
            case $this->edit:
                $result = $isManager || $isEditor || ($isAuthor && $isSameUser);
                break;
            case $this->delete:
            case $this->archive:
                $result = $isManager || ($isAuthor && $isSameUser);
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Ticket;
    }
}