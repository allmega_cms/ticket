<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Security;

use Allmega\TicketBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\TicketBundle\Entity\Project;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ProjectVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'ticket-project');
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        $isUser = $this->hasRole($user, Data::PROJECT_USER_ROLE);
        if (!$isUser || !$this->isSettedAndSupports($attribute, $subject)) return false;

        $isMember = $subject && $subject->getEmployees()->contains($user);
        $isManager = $this->hasRole($user, Data::PROJECT_MANAGER_ROLE);
        $isAuthor = $this->hasRole($user, Data::PROJECT_AUTHOR_ROLE);

        switch ($attribute) {
            case $this->dashboard:
            case $this->search:
            case $this->list:
                $result = true;
                break;
            case $this->files:
            case $this->add:
                $result = $isAuthor;
                break;
            case $this->show:
                $result = $isManager || $isMember;
                break;
            case $this->delete:
            case $this->state:
            case $this->edit:
                $result = $isManager || $isAuthor;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Project;
    }
}