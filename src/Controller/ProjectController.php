<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Controller;

use Allmega\TicketBundle\{Data, Events};
use Allmega\TicketBundle\Entity\Project;
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\TicketBundle\Security\ProjectVoter;
use Allmega\BlogBundle\Utils\Params\TemplateParams;
use Allmega\BlogBundle\Utils\{Paginator, Params\RepoParams, Params\UTypes, SortableItem};
use Allmega\TicketBundle\Repository\ProjectRepository;
use Allmega\TicketBundle\Manager\ProjectControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/project', name: 'allmega_ticket_project_')]
class ProjectController extends BaseController
{
    use ProjectControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaTicket/project/';
    public const ROUTE_NAME = 'allmega_ticket_project_';
    public const PROP = 'project';

    public function __construct(
        private readonly ProjectRepository $projectRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search/{type}', name: 'search', requirements: ['type' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('ticket-project-search')]
    public function search(SearchItem $item, string $type = UTypes::MEMBER): Response
    {
        $type = $this->checkType($type);
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setRouteParams(['type' => $type])
            ->setProps(['title'])
            ->getRepo()
            ->setSearch($this->projectRepo)
            ->addOptions(['user' => $this->getUser(), 'type' => $type]);
        
        $item->getTrans()->setLabel('project.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list/{id}', name: 'index', requirements: ['id' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('ticket-project-list')]
    public function index(ProjectVoter $projectVoter, Paginator $paginator, string $id = UTypes::MEMBER): Response
    {
        $type = $this->checkType($id);
        $repoParams = new RepoParams($this->getUser(), $type);
        $query = $this->projectRepo->findAllQuery($repoParams);

        $optParams = [
            'data' => new TemplateParams(self::PROP, $type),
            'projectVoter' => $projectVoter,
        ];

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'item' => SortableItem::getInstance(new Project()),
            'projects' => $paginator->getPagination($query),
        ]);
    }

    #[Route('/show/{id}/{type}', name: 'show', requirements: ['type' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('ticket-project-show', subject: self::PROP)]
    public function show(ProjectVoter $projectVoter, Project $project, string $type = UTypes::MEMBER): Response
    {
        $type = $this->checkType($type);
        $optParams = [
            'data' => new TemplateParams(self::PROP, $type, true),
            'projectVoter' => $projectVoter,
            'entityname' => Project::class,
            'project' => $project,
        ];

        $params = [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams)
        ];

        if ($this->isXmlRequest) {
            $params['data']->setShow(false);
            $data = [
                'content' => $this->renderView(self::ROUTE_TEMPLATE_PATH . 'show/_layout.html.twig', $params),
            ];
            return $this->json($data);
        }
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', $params);
    }

   #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
   #[IsGranted('ticket-project-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: 'GET|POST')]
    #[IsGranted('ticket-project-edit', subject: self::PROP)]
    public function edit(Project $project): Response
    {
        return $this->save($project);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('ticket-project-delete', subject: self::PROP)]
    public function delete(Project $project): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $project,
            domain: Data::DOMAIN,
            eventName: Events::PROJECT_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('ticket-project-state', subject: self::PROP)]
    public function changeState(Project $project): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $project,
            domain: Data::DOMAIN,
            eventName: Events::PROJECT_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('ticket-project-dashboard')) return new Response();
        
        $user = $this->getUser();
        $latestNum = $this->projectRepo->countLatest($user);

        $repoParams = new RepoParams($user, count: true);
        $num = $this->projectRepo->findByRepoParams($repoParams);

        $repoParams->setCount(false)->setLimit(3);
        $projects = $this->projectRepo->findByRepoParams($repoParams);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'projects' => $projects, 'latestNum' => $latestNum, 'num' => $num,
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }

    private function checkType(string $type): string
    {
        if ($type === UTypes::MANAGER && $this->isGranted(Data::PROJECT_MANAGER_ROLE)) {
            return $type;
        } elseif ($type === UTypes::AUTHOR && $this->isGranted(Data::PROJECT_AUTHOR_ROLE)) {
            return $type;
        } else {
            return UTypes::MEMBER;
        }
    }
}