<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Controller;

use Allmega\TicketBundle\{Data, Events};
use Allmega\TicketBundle\Entity\{Ticket, TicketState};
use Allmega\TicketBundle\Manager\TicketStateControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/state', name: 'allmega_ticket_state_')]
class TicketStateController extends BaseController
{
    use TicketStateControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaTicket/ticketstate/';
    public const ROUTE_NAME = 'allmega_ticket_state_';
    public const PROP = 'state';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/{id}/add', name: 'add', methods: ['GET','POST'])]
    #[IsGranted('ticket-state-add', subject: 'ticket')]
    public function add(Ticket $ticket): Response
    {
        return $this->save(null, $ticket);
    }

    #[Route('/{id}/edit', name: 'edit', methods: ['GET','POST'])]
    #[IsGranted('ticket-state-edit', subject: self::PROP)]
    public function edit(TicketState $state): Response
    {
        return $this->save($state);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('ticket-state-delete', subject: self::PROP)]
    public function delete(TicketState $state): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $state,
            domain: Data::DOMAIN,
            eventName: Events::TICKETSTATE_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/{id}/state', name: 'state', methods: 'GET')]
    #[IsGranted('ticket-state-state', subject: self::PROP)]
    public function changeState(TicketState $state): Response
    {
        if (!$state->getReadedAt()) {
            $state->setReadedAt();
            $this->getEntityManager()->persist($state);
            $this->getEntityManager()->flush();
        }
        return $this->redirectToRoute(TicketController::ROUTE_NAME . $this->show, [
            'id' => $state->getTicket()->getId()
        ], Response::HTTP_SEE_OTHER);
    }
}