<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Controller;

use Allmega\TicketBundle\Entity\Ticket;
use Allmega\TicketBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\TicketBundle\Repository\TicketRepository;
use Allmega\TicketBundle\Manager\TicketControllerTrait;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\TicketBundle\Utils\{Absence, TemplateParams};
use Allmega\TicketBundle\Security\{TicketVoter, TicketStateVoter};
use Allmega\BlogBundle\Utils\Params\{BaseControllerParams, RepoParams, UTypes};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(name: 'allmega_ticket_')]
class TicketController extends BaseController
{
    use TicketControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaTicket/ticket/';
    public const ROUTE_NAME = 'allmega_ticket_';
    public const PROP = 'ticket';

    public function __construct(
        private readonly TicketRepository $ticketRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search/{type}', name: 'search', requirements: ['type' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('ticket-search')]
    public function search(SearchItem $item, string $type = UTypes::MEMBER): Response
    {
        $type = $this->checkType($type);
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setRouteParams(['type' => $type])
            ->setProps(['title'])
            ->getRepo()
            ->setSearch($this->ticketRepo)
            ->addOptions(['type' => $type, 'user' => $this->getUser()]);
        
        $item->getTrans()->setLabel('ticket.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list/{id}', name: 'index', requirements: ['id' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('ticket-list')]
    public function index(
        TicketVoter $ticketVoter,
        Paginator $paginator,
        Absence $absence,
        string $id = UTypes::MEMBER): Response
    {
        $type = $this->checkType($id);
        $repoParams = new RepoParams($this->getUser(), $type);
        $query = $this->ticketRepo->findAllQuery($repoParams);

        $optParams = [
            'data' => new TemplateParams($type),
            'ticketVoter' => $ticketVoter,
            'absence' => $absence];

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'item' => SortableItem::getInstance(new Ticket()),
            'tickets' => $paginator->getPagination($query),
        ]);
    }

    #[Route('/show/{id}/{type}', name: 'show', requirements: ['type' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('ticket-show', subject: self::PROP)]
    public function show(
        TicketStateVoter $ticketStateVoter,
        TicketVoter $ticketVoter,
        Absence $absence,
        Ticket $ticket,
        string $type = UTypes::MEMBER): Response
    {
        $type = $this->checkType($type);
        $optParams = [
            'ticketstate' => [
                'path' => TicketStateController::ROUTE_TEMPLATE_PATH,
                'route' => TicketStateController::ROUTE_NAME,
            ],
            'data' => new TemplateParams($type, true),
            'ticketStateVoter' => $ticketStateVoter,
            'ticketVoter' => $ticketVoter,
            'absence' => $absence,
            'ticket' => $ticket,
        ];

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams)
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET','POST'])]
    #[IsGranted('ticket-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET','POST'])]
    #[IsGranted('ticket-edit', subject: self::PROP)]
    public function edit(Ticket $ticket): Response
    {
        return $this->save($ticket);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('ticket-delete', subject: self::PROP)]
    public function delete(Ticket $ticket): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $ticket,
            domain: Data::DOMAIN,
            eventName: Events::TICKET_DELETED,
            routeName: self::ROUTE_NAME,
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/archive/{id}', name: 'archive', methods: 'GET')]
    #[IsGranted('ticket-archive', subject: self::PROP)]
    public function archive(Ticket $ticket): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $ticket,
            domain: Data::DOMAIN,
            eventName: Events::TICKET_ARCHIVED,
            method: 'archived',
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('ticket-dashboard')) return new Response();
        
        $user = $this->getUser();
        $latestNum = $this->ticketRepo->countLatest($user);

        $repoParams = new RepoParams($user, count: true);
        $num = $this->ticketRepo->findByRepoParams($repoParams);

        $repoParams->setCount(false)->setLimit(3);
        $tickets = $this->ticketRepo->findByRepoParams($repoParams);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'tickets' => $tickets, 'latestNum' => $latestNum, 'num' => $num,
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }

    private function checkType(string $type): string
    {
        if ($type == UTypes::MANAGER && $this->isGranted(Data::TICKET_MANAGER_ROLE)) {
            return $type;
        } elseif (($type == UTypes::AUTHOR || $type == UTypes::ARCHIVE) && $this->isGranted(Data::TICKET_AUTHOR_ROLE)) {
            return $type;
        } else {
            return UTypes::MEMBER;
        }
    }
}