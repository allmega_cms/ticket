<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\EventSubscriber;

use Allmega\TicketBundle\Events;
use Allmega\BlogBundle\Model\FlashesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlashSubscriber implements EventSubscriberInterface
{
    use FlashesTrait;

    public static function getSubscribedEvents(): array
    {
        return [
            Events::TICKET_CREATED => 'addSuccessFlash',
            Events::TICKET_UPDATED => 'addSuccessFlash',
            Events::TICKET_DELETED => 'addSuccessFlash',
            Events::TICKET_ARCHIVED => 'addSuccessFlash',
            Events::TICKET_ASSIGNED => 'addSuccessFlash',
            Events::TICKET_COMPLETED => 'addSuccessFlash',
            Events::TICKETSTATE_CREATED => 'addSuccessFlash',
            Events::TICKETSTATE_UPDATED => 'addSuccessFlash',
            Events::TICKETSTATE_DELETED => 'addSuccessFlash',
        ];
    }
}