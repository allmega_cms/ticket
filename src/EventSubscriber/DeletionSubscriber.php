<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\EventSubscriber;

use Allmega\BlogBundle\Entity\Tag;
use Allmega\AuthBundle\Entity\User;
use Allmega\TicketBundle\Entity\Project;
use Allmega\TicketBundle\Entity\Ticket;
use Allmega\AuthBundle\Events as AuthEvents;
use Allmega\BlogBundle\Events as BlogEvents;
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Doctrine\ORM\EntityManagerInterface;

readonly class DeletionSubscriber implements EventSubscriberInterface
{
    public function __construct(private EntityManagerInterface $em) {}

    public static function getSubscribedEvents(): array
    {
        return [
            AuthEvents::USER_DELETE => 'handleUserRelations',
            BlogEvents::TAG_DELETED => 'handleTagRelations',
        ];
    }

    public function handleUserRelations(GenericEvent $event, string $eventName): void
    {
        $userRepo = $this->em->getRepository(User::class);
        $user = $event->getSubject();

        $userRepo
            ->updateField(Ticket::class, 'author', $user)
            ->deleteUserRelations('allmega_ticket__user', $user)
            ->updateField(Project::class, 'creator', $user)
            ->updateField(Project::class, 'editor', $user);

        $module = ['project'];
        foreach ($module as $modul) {
            $table = 'allmega_ticket__' . $modul . '_user';
            $userRepo->deleteUserRelations($table, $user);
        }
    }

    public function handleTagRelations(GenericEvent $event, string $eventName): void
    {
        $this->em->getRepository(Tag::class)->deleteTagRelations('allmega_ticket__tag', $event);
    }
}