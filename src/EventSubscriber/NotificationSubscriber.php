<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\EventSubscriber;

use Allmega\TicketBundle\{Data, Events};
use Allmega\TicketBundle\Utils\Absence;
use Allmega\BlogBundle\Utils\FlashBag;
use Allmega\TicketBundle\Entity\Ticket;
use Allmega\BlogBundle\Utils\Params\NotificationParams;
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class NotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MessageBusInterface $bus,
        private FlashBag $flashbag,
        private Absence $absence) {}

    public static function getSubscribedEvents(): array
    {
        return [
            Events::PROJECT_ENABLED => 'onProjectEnabled',
            Events::TICKET_CREATED   => 'onTicketCreated',
            Events::TICKET_UPDATED => 'onTicketUpdated',
            Events::TICKET_ASSIGNED  => 'onTicketAssigned',
            Events::TICKET_COMPLETED => 'onTicketCompleted',
            Events::TICKETSTATE_CREATED => 'onTicketStateChanged',
            Events::TICKETSTATE_UPDATED => 'onTicketStateChanged'
        ];
    }

    /**
     * @throws ExceptionInterface
     */
    public function onProjectEnabled(GenericEvent $event): void
    {
        $project = $event->getSubject();
        $author = $project->getCreator();

        $receivers = [];
        foreach ($project->getEmployees() as $employee) {
            $receivers[] = $employee->getEmail();
        }

        $search  = ['%username%', '%title%', '%description%'];
        $replace = [
            $author->getFullname(),
            $project->getTitle(),
            $project->getDescription()
        ];

        $params = new NotificationParams(Data::NOTIFICATION_PROJECT_ENABLED, $receivers, $search, $replace);
        $this->bus->dispatch($params);
    }

    /**
     * @throws ExceptionInterface
     */
    public function onTicketCreated(GenericEvent $event): void
    {
        $ticket = $event->getSubject();
        $this->handleTicket($ticket);

        $search  = ['%num%'];
        $replace = [$ticket->getNum()];

        $params = new NotificationParams(Data::NOTIFICATION_TICKET_CREATED, [], $search, $replace);
        foreach ($ticket->getEditors() as $editor) $params->addReceiver($editor->getEmail());
        $this->bus->dispatch($params);
    }

    /**
     * @throws ExceptionInterface
     */
    public function onTicketUpdated(GenericEvent $event): void
    {
        $this->handleTicket($event->getSubject());

    }

    /**
     * @throws ExceptionInterface
     */
    public function onTicketAssigned(GenericEvent $event): void
    {
        $ticket = $event->getSubject();
        $search = ['%num%'];
        $replace = [$ticket->getNum()];

        $params = new NotificationParams(Data::NOTIFICATION_TICKET_ASSIGNED, [], $search, $replace);
        foreach ($ticket->getEditors() as $editor) $params->addReceiver($editor->getEmail());
        $this->bus->dispatch($params);
    }

    /**
     * @throws ExceptionInterface
     */
    public function onTicketCompleted(GenericEvent $event): void
    {
        $ticket = $event->getSubject();
        $author = $ticket->getAuthor();

        $search  = ['%username%', '%num%'];
        $replace = [$author->getFullname(), $ticket->getNum()];

        $params = new NotificationParams(Data::NOTIFICATION_TICKET_COMPLETED, [$author->getEmail()], $search, $replace);
        $this->bus->dispatch($params);
    }

    /**
     * @throws ExceptionInterface
     */
    public function onTicketStateChanged(GenericEvent $event): void
    {
        $state = $event->getSubject();
        $ticket = $state->getTicket();
        $author = $ticket->getAuthor();

        $search  = ['%username%', '%num%', '%employee%', '%message%'];
        $replace = [
            $author->getFullname(),
            $ticket->getNum(),
            $state->getAuthor()->getFullname(),
            $state->getMessage(),
        ];

        $params = new NotificationParams(Data::NOTIFICATION_TICKET_STATE_CHANGED, [$author->getEmail()], $search, $replace);
        $this->bus->dispatch($params);
    }

    /**
     * @throws ExceptionInterface
     */
    private function handleTicket(Ticket $ticket): void
    {
        $phrase = $this->absence->getPhrase($ticket);
        if ($phrase) {
            $this->flashbag->add('warning', nl2br($phrase), Data::DOMAIN);
            $author = $ticket->getAuthor();
            $search = ['%username%', '%num%', '%absent_employees%'];
            $replace = [
                $author->getFullname(),
                $ticket->getNum(), 
                $phrase
            ];

            $params = new NotificationParams(Data::NOTIFICATION_ABSENT_EMPLOYEES, [$author->getEmail()], $search, $replace);
            $this->bus->dispatch($params);
        }
    }
}