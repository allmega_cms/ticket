<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Entity;

use Allmega\BlogBundle\Entity\Tag;
use Allmega\AuthBundle\Entity\User;
use Allmega\MediaBundle\Model\MediaInterface;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Model\SortableItemInterface;
use Allmega\TicketBundle\Repository\TicketRepository;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

#[ORM\Entity(repositoryClass: TicketRepository::class)]
#[ORM\Table(name: '`allmega_ticket`')]
class Ticket implements MediaInterface, SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    private ?string $num = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Assert\Length(max: 10000, maxMessage: 'errors.max_value')]
    private ?string $description = null;

    #[ORM\OneToMany(targetEntity: TicketState::class, mappedBy: 'ticket', orphanRemoval: true)]
    #[ORM\OrderBy(['updated' => 'DESC'])]
    private Collection $states;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $report = null;

    #[ORM\ManyToOne]
    private ?User $reporter = null;

    #[ORM\ManyToOne(inversedBy: 'tickets')]
    private ?Project $project = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $reported = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $created;

    #[ORM\Column]
    private int $prio = 0;

    #[ORM\Column]
    private bool $done = false;

    #[ORM\Column]
    private bool $assigned = false;

    #[ORM\Column]
    private bool $archived = false;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $author = null;

    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: '`allmega_ticket__user`')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    private Collection $editors;

    #[Assert\Count(max: '4', maxMessage: 'errors.too_many_tags')]
    #[ORM\ManyToMany(targetEntity: Tag::class, cascade: ['persist'])]
    #[ORM\JoinTable(name: '`allmega_ticket__tag`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $tags;
    
    private array $mediatypes = [];
    private string $mediaDir = '';

    /**
     * Create a new Ticket entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $description, $title as dummy text
     * - $author will be created
     */
    public static function build(
        string $description = null,
        string $title = null,
        User $author = null,
        array $editors = []): static
    {
        $description = $description ?? Helper::getLoremIpsum();
        $title = $title ?? Helper::generateRandomString();
        $author = $author ?? User::build();
        $num = '#' . uniqid();

        $ticket = (new static())
            ->setDescription($description)
            ->setAuthor($author)
            ->setTitle($title)
            ->setNum($num);

        foreach ($editors as $editor) $ticket->addEditor($editor);
        return $ticket;
    }

    public function __construct()
    {
        $this->created = new DateTime();
        $this->editors = new ArrayCollection();
        $this->states = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNum(): ?string
    {
        return $this->num;
    }

    public function setNum(string $num): static
    {
        $this->num = $num;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection<int, TicketState>
     */
    public function getStates(): Collection
    {
        return $this->states;
    }

    public function addState(TicketState $state): static
    {
        if (!$this->states->contains($state)) {
            $this->states->add($state);
            $state->setTicket($this);
        }

        return $this;
    }

    public function removeState(TicketState $state): static
    {
        if ($this->states->removeElement($state)) {
            // set the owning side to null (unless already changed)
            if ($state->getTicket() === $this) {
                $state->setTicket(null);
            }
        }

        return $this;
    }

    public function getReport(): ?string
    {
        return $this->report;
    }

    public function setReport(?string $report): static
    {
        $this->report = $report;
        return $this;
    }

    public function getReporter(): ?User
    {
        return $this->reporter;
    }

    public function setReporter(?User $reporter): static
    {
        $this->reporter = $reporter;
        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): static
    {
        $this->project = $project;
        return $this;
    }

    public function getReported(): ?DateTimeInterface
    {
        return $this->reported;
    }

    public function setReported(): static
    {
        $this->reported = new DateTime();
        return $this;
    }

    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    public function getPrio(): int
    {
        return $this->prio;
    }

    public function setPrio(int $prio): static
    {
        $this->prio = $prio;
        return $this;
    }

    public function getDone(): bool
    {
        return $this->done;
    }

    public function setDone(bool $done): static
    {
        $this->done = $done;
        return $this;
    }

    public function getAssigned(): bool
    {
        return $this->assigned;
    }

    public function setAssigned(bool $assigned): static
    {
        $this->assigned = $assigned;
        return $this;
    }

    public function isArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): static
    {
        $this->archived = $archived;
        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): static
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return Collection<int,User>
     */
    public function getEditors(): Collection
    {
        return $this->editors;
    }

    public function addEditor(User $editor): static
    {
        if (!$this->editors->contains($editor)) $this->editors[] = $editor;
        return $this;
    }

    public function removeEditor(User $editor): static
    {
        if ($this->editors->contains($editor)) $this->editors->removeElement($editor);
        return $this;
    }

    /**
     * @return Collection<int,Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag ...$tags): void
    {
        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) $this->tags->add($tag);
        }
    }

    public function removeTag(Tag $tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function getMediatypes(): array
    {
        return $this->mediatypes;
    }

    public function setMediatypes(array $mediatypes): static
    {
        $this->mediatypes = $mediatypes;
        return $this;
    }

    public function getMediaDir(): string
    {
        if (!$this->mediaDir) $this->setMediaDir();
        return $this->mediaDir;
    }

    public function setMediaDir(): static
    {
        $this->mediaDir = 'tickets_'.$this->id;
        return $this;
    }

    public static function getSortableProps(): array
    {
        return ['title', 'created', 'prio', 'done'];
    }

    public static function getBundleName(): string
    {
        return 'Ticket';
    }
}