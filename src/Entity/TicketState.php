<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Entity;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\TicketBundle\Repository\TicketStateRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

#[ORM\Entity(repositoryClass: TicketStateRepository::class)]
#[ORM\Table(name: '`allmega_ticket__state`')]
class TicketState
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 3000, maxMessage: 'errors.max_value')]
    private ?string $message = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $author = null;

    #[ORM\Column]
    private int $duration = 0;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $updated;

    #[ORM\ManyToOne(inversedBy: 'states')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Ticket $ticket = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $readedAt = null;

    /**
     * Create a new TicketState entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $author, $ticket will be created
     * - $message as dummy text
     */
    public static function build(
        string $message = null,
        Ticket $ticket = null,
        User $author = null): static
    {
        $author = $author ?? User::build();
        $ticket = $ticket ?? Ticket::build(author: $author);
        $message = $message ?? Helper::generateRandomString();

        return (new static())
            ->setMessage($message)
            ->setAuthor($author)
            ->setTicket($ticket);
    }

    public function __construct()
    {
        $this->updated = new DateTime();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): static
    {
        $this->message = $message;
        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): static
    {
        $this->author = $author;
        return $this;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): static
    {
        $this->duration = $duration;
        return $this;
    }

    public function getUpdated(): ?DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(): static
    {
        $this->updated = new DateTime();
        return $this;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setTicket(?Ticket $ticket): static
    {
        $this->ticket = $ticket;
        return $this;
    }

    public function getReadedAt(): ?DateTimeInterface
    {
        return $this->readedAt;
    }

    public function setReadedAt(bool $reset = false): static
    {
        $this->readedAt = $reset ? null : new DateTime();
        return $this;
    }
}