<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Entity;

use Allmega\BlogBundle\Entity\Tag;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\TicketBundle\Repository\ProjectRepository;
use Allmega\BlogBundle\Model\{ItemInfo, SortableItemInterface};
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
#[ORM\Table(name: '`allmega_ticket__project`')]
class Project extends ItemInfo implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\ManyToOne]
    private ?Project $parent = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Assert\Length(max: 3000, maxMessage: 'errors.max_value')]
    private ?string $description = null;

    /**
     * @var Collection<int,User>
     */
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: '`allmega_ticket__project_user`')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    private Collection $employees;

    /**
     * @var Collection<int,Ticket>
     */
    #[ORM\OneToMany(targetEntity: Ticket::class, mappedBy: 'project')]
    private Collection $tickets;

    #[Assert\Count(max: 4, maxMessage: 'errors.too_many_tags')]
    #[ORM\ManyToMany(targetEntity: Tag::class, cascade: ['persist'])]
    #[ORM\JoinTable(name: '`allmega_ticket__project_tag`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $tags;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;

    #[ORM\Column]
    private int $prio = 0;

    /**
     * @var int $duration Duration of current Project in seconds
     */
    private int $duration = 0;

    /**
     * Create a new Project entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $description, $title as dummy text
     * - $editor will be created
     */
    public static function build(
        string $description = null,
        string $title = null,
        User $user = null,
        array $employees = []): static
    {
        $description = $description ?? Helper::getLoremIpsum();
        $title = $title ?? Helper::generateRandomString(50);
        $user = $user ?? User::build();

        $project = (new static())
            ->setDescription($description)
            ->setCreator($user)
            ->setEditor($user)
            ->setTitle($title);
        
        foreach ($employees as $employee) $project->addEmployee($employee);
        return $project;
    }

    public function __construct()
    {
        parent::__construct();
        $this->tickets = new ArrayCollection();
        $this->employees = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?Project $parent): static
    {
        $this->parent = $parent;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }

    public function addEmployee(User $employee): static
    {
        if (!$this->employees->contains($employee)) {
            $this->employees->add($employee);
        }
        return $this;
    }

    public function removeEmployee(User $employee): static
    {
        $this->employees->removeElement($employee);
        return $this;
    }

    /**
     * @return Collection<int,Ticket>
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): static
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets->add($ticket);
            $ticket->setProject($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): static
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getProject() === $this) {
                $ticket->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int,Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag ...$tags): void
    {
        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) $this->tags->add($tag);
        }
    }

    public function removeTag(Tag $tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function getPrio(): int
    {
        return $this->prio;
    }

    public function setPrio(int $prio): static
    {
        $this->prio = $prio;
        return $this;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setDuration(): static
    {
        $this->duration = 0;
        foreach ($this->tickets as $ticket) {
            foreach($ticket->getStates() as $state) {
                $this->duration += $state->getDuration();
            }
        }

        if ($this->duration > 0) {
            $minutes = floor($this->duration / 60);
            $hours = $minutes % 60;
            $times = $hours + ($minutes - $hours * 60) / 60;
            $this->duration = round($times, 2);
        }
        return $this;
    }

    public function setMediaDir(): void
    {
        $this->mediaDir = 'projects_'.$this->id;
    }

    public static function getSortableProps(): array
    {
        return ['title', 'created', 'updated', 'active', 'prio'];
    }

    public static function getBundleName(): string
    {
        return 'Ticket';
    }
}