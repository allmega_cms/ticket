<?php

/**
 * This file is part of the Allmega Ticket Bundle package.
 *
 * @copyright Allmega 
 * @package   Ticket Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\TicketBundle\Twig;

use Allmega\TicketBundle\Utils\Twig\{CreatorInfo, TicketState};
use Twig\Extension\AbstractExtension;
use Twig\{TwigFunction, Environment};

class TicketExtension extends AbstractExtension
{
    private const TEMPLATE_PATH = '@AllmegaTicket/twig/';

    public function getFunctions(): array
    {
        return [
            new TwigFunction('allmega_render_ticket_creator', [$this, 'creatorInfo'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_ticket_state', [$this, 'ticketState'], ['is_safe' => ['html'], 'needs_environment' => true]),
        ];
    }

    public function creatorInfo(Environment $env, array $params): string
    {
        return (new CreatorInfo($env, $params))->getView();
    }

    public function ticketState(Environment $env, array $params): string
    {
        return (new TicketState($env, $params))->getView();
    }
}